<?php
$servername = "mysql";
$username = "root";
$password = "test";
$dbname = "information_schema";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

$sql = "SELECT * FROM TABLES";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
        echo "row: " . json_encode($row) ."<br>";
    }
} else {
    echo "0 results";
}
$conn->close();